---
layout: markdown_page
title: "GitLab adds Emerasoft to Global Partnership Program"
---

**San Francisco (Jul 31st, 2017)** – Today, GitLab announced a partnership with Emerasoft Srl., an Italian company providing software development products, consulting and training services. Emerasoft specializes in solving complex technical challenges, especially in the areas of ALM, DevOps, Software Testing, Security, IoT, and Business Process Intelligence. Emerasoft will now sell and support GitLab products as part of their portfolio. The team will provide support to customers during their onboarding process, helping them not only take advantage of GitLab’s repository management, but also the product’s issue management, continuous integration, and continuous delivery capabilities. 

“GitLab can help our customers solve the problem of having to manage different DevOps tools and provide them with a single software where to integrate the different activities,”  says Riccardo Bernasconi, Emerasoft Sales Manager. 

“Emerasoft has a wealth of experience working in the software development products and services space. With their experience in ALM and DevOps, Emerasoft is advantageously positioned to support Italian-based customers in their efforts to adopt GitLab for their full software development lifecycle,” says Michael Alessio, GitLab’s Director of Global Alliances.

Emerasoft Srl. joins a growing community of GitLab partners, around the world. For a complete list of GitLab resellers, please visit [https://about.gitlab.com/resellers/](https://about.gitlab.com/resellers/).

**About Emerasoft Srl.** 

Founded in 2005, Emerasoft has strong expertise in a variety of different areas of the IT business, ranging from ALM and DevOps to Software Testing and IoT. The team's expertise is reflected in the their ability to deal with heterogeneous and complex problems in professional and qualified way. Emerasoft works with customers to select state-of-the-art technologies, navigate new frontiers of application development, and build a path of excellence and quality.

For more information about Emerasoft Srl., please visit:
- [Emerasoft website](https://www.emerasoft.com) or write to `gitlab@emerasoft.com`
- [GitLab-specific webpage](http://www.emerasoft.com/devops-servizi-consulenza-strumenti/gitlab/)  
- [Emerasoft Facebook page](https://www.facebook.com/emerasoft)
- [Emerasoft Twitter page](https://twitter.com/emerasoft)
- [Emerasoft LinkedIn page](https://www.linkedin.com/company-beta/1203309/)

**About GitLab**

Since its founding in 2014, GitLab has quickly become the leading self-hosted Git repository management tool used by software development teams ranging from startups to global enterprise organizations. GitLab has since expanded its product offering to deliver an integrated source code management, code review, test/release automation, and application monitoring platform that accelerates and simplifies the software development process. With one end-to-end software development platform, GitLab helps teams eliminate unnecessary steps from their workflow and focus exclusively on building great software. Today, more than 100,000 organizations, including NASA, CERN, Alibaba, SpaceX, O'Reilly, IBM and ING, trust GitLab to bring their modern applications from idea to production, reliably and repeatedly.